package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"sync"
)

// TRIHARD

var gifs = []string{"alert", "still", "nrun1", "nrun2", "nerun1", "nerun2",
	"erun1", "erun2", "serun1", "serun2", "srun1", "srun2", "swrun1", "swrun2",
	"wrun1", "wrun2", "nwrun1", "nwrun2", "yawn", "sleep1", "sleep2", "itch1",
	"itch2", "nscratch1", "nscratch2", "escratch1", "escratch2", "sscratch1",
	"sscratch2", "wscratch1", "wscratch2",
}

var nekos = []string{"white", "black", "gray", "calico", "robot", "peach",
	"colourful", "earth", "air", "water", "fire", "spirit", "rainbow",
	"silversky", "orange", "ghetto", "neon", "pink", "ghost", "lucky", "moka",
	"usa", "rose", "blue", "silver", "kuramecha", "kina", "ace", "spooky",
	"holiday", "valentine", "marmalade", "royal", "mermaid", "socks", "dave",
	"jess", "mike", "lucy", "fancy",
}

func main() {
	s := "https://webneko.net/%s/%s.gif"

	wg := sync.WaitGroup{}

	for _, n := range nekos {
		_ = os.Mkdir(n, os.ModePerm)
		for _, g := range gifs {
			wg.Add(1)
			go func(n, g string) {
				resp, err := http.Get(fmt.Sprintf(s, n, g))
				must(err)

				f, err := os.Create(filepath.Join(n, g+".gif"))
				must(err)

				_, err = io.Copy(f, resp.Body)
				must(err)

				resp.Body.Close()
				wg.Done()
			}(n, g)
		}
	}
	wg.Wait()
}

func must(err error) {
	if err != nil {
		panic(err)
	}
}
