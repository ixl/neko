build: index/main.wasm index/wasm_exec.js

index/main.wasm: main/*.go *.go
	GOOS=js GOARCH=wasm GOOS=js GOARCH=wasm go build -o index/main.wasm ./main


index/wasm_exec.js:
	cp $(shell go env GOROOT)/misc/wasm/wasm_exec.js index


clear:
	-rm -rf index/main.wasm

serve: index/main.wasm index/wasm_exec.js
	go get -u github.com/shurcooL/goexec
	goexec 'http.ListenAndServe(`:1337`, http.FileServer(http.Dir(`index`)))'

build-serve: clear build
	go build -o server ./serve

docker:	clear
	docker build -t neko .