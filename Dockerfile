FROM golang:latest

WORKDIR /app

COPY . /app

RUN rm index/wasm_exec.js
RUN make build-serve

CMD "./server" $PORT