package neko

import (
	"math"
	"math/cmplx"
	"math/rand"
	"time"
)

type Constants struct {
	// step
	Step float64

	//max distance from cursor
	Dist float64

	//Frames to wait
	StillFrames   int
	YawnFrames    int
	ItchFrames    int
	ScratchFrames int
}

func Default() (State, Data, Constants) {
	return StateStill{}, Data{},
		Constants{
			Step:          15,
			Dist:          20,
			StillFrames:   2,
			YawnFrames:    3,
			ItchFrames:    4,
			ScratchFrames: 4,
		}
}

type Point struct {
	// TODO change to float64?
	X, Y int
}

type Data struct {
	Mouse Point
	Neko  Point
}

func UpdateMouseData(d Data, p Point) Data {
	return Data{Neko: d.Neko, Mouse: p}
}

// TODO rename to make step?
func UpdateNekoData(d Data, c Constants) Data {
	p := d.Neko
	phi := argument(d)

	p.X += int(c.Step * math.Cos(phi))
	p.Y += int(c.Step * math.Sin(phi))
	return Data{Mouse: d.Mouse, Neko: p}
}

type State interface {
	Next(Data, Constants) (State, Data)
	Image(Data) string
}

type StateStill struct {
	frame int
	// # of frames idle
	// includes yawn and shit
	yawned bool
}

func (s StateStill) Next(d Data, c Constants) (State, Data) {
	s.frame++

	if !isIn(d, c) {
		return StateAlert{}, d
	}
	if s.yawned {
		return StateSleep{}, d
	}
	if s.frame > c.StillFrames {
		// with 0.5 probabilty going to itch
		rand.Seed(time.Now().Unix())
		r := rand.Intn(3)
		switch r {
		case 0:
			return StateYawn{}, d
		case 1:
			return StateItch{}, d
		case 2:
			return newStateScratch(), d
		}
	}

	return s, d
}

func (s StateStill) Image(d Data) string {
	return "still"
}

type StateAlert struct{}

func (s StateAlert) Next(d Data, c Constants) (State, Data) {
	// TODO initialize run
	// maybe new func?
	return StateRun{}, d
}

func (s StateAlert) Image(d Data) string {
	return "alert"
}

type StateRun struct {
	// Todo enum
	frame bool
}

func (s StateRun) Next(d Data, c Constants) (State, Data) {
	// Todo move to func
	if isIn(d, c) {
		return StateStill{}, d
	}
	d = UpdateNekoData(d, c)
	runState := nextRunState(s, d)
	return runState, d
}

func (s StateRun) Image(d Data) string {
	frame := "1"
	if s.frame {
		frame = "2"
	}
	prefix := angle(d)
	return prefix + "run" + frame
}

func nextRunState(s StateRun, d Data) State {
	s.frame = !s.frame
	return s
}

var (
	DirectionE  = "s"
	DirectionNE = "se"
	DirectionN  = "e"
	DirectionNW = "ne"
	DirectionW  = "n"
	DirectionSW = "nw"
	DirectionS  = "w"
	DirectionSE = "sw"
)

var pi = math.Pi

func angle(d Data) string {
	phi := argument(d)
	// warning shit code ahead
	switch {
	case 0*pi/8 <= phi && phi <= 1*pi/8 || 14*pi/8 <= phi && phi <= 16*pi/8:
		return DirectionE
	case 1*pi/8 <= phi && phi <= 3*pi/8:
		return DirectionNE
	case 3*pi/8 <= phi && phi <= 5*pi/8:
		return DirectionN
	case 5*pi/8 <= phi && phi <= 7*pi/8:
		return DirectionNW
	case 7*pi/8 <= phi && phi <= 9*pi/8:
		return DirectionW
	case 9*pi/8 <= phi && phi <= 11*pi/8:
		return DirectionSW
	case 11*pi/8 <= phi && phi <= 13*pi/8:
		return DirectionS
	case 13*pi/8 <= phi && phi <= 15*pi/8:
		return DirectionSE
	}
	return "unrecognized"
}

func argument(d Data) float64 {
	v := complex(float64(d.Mouse.X-d.Neko.X), float64(d.Mouse.Y-d.Neko.Y))
	phi := cmplx.Phase(v)
	if phi < 0 {
		phi += 2 * pi
	}
	return phi
}

func isIn(d Data, c Constants) bool {
	dx := float64(d.Mouse.X - d.Neko.X)
	dy := float64(d.Mouse.Y - d.Neko.Y)
	r := math.Hypot(dx, dy)
	return r < c.Dist
}

var Gifs = []string{"alert", "still", "nrun1", "nrun2", "nerun1", "nerun2",
	"erun1", "erun2", "serun1", "serun2", "srun1", "srun2", "swrun1", "swrun2",
	"wrun1", "wrun2", "nwrun1", "nwrun2", "yawn", "sleep1", "sleep2", "itch1",
	"itch2", "nscratch1", "nscratch2", "escratch1", "escratch2", "sscratch1",
	"sscratch2", "wscratch1", "wscratch2"}

type StateYawn struct {
	// making it slightly long
	frame int
}

func (s StateYawn) Next(d Data, c Constants) (State, Data) {
	s.frame++

	if s.frame > c.YawnFrames {
		if isIn(d, c) {
			return StateStill{yawned: true}, d
		}
		return StateAlert{}, d
	}
	return s, d
}

func (s StateYawn) Image(d Data) string {
	return "yawn"
}

type StateSleep struct {
	frame bool
}

func (s StateSleep) Next(d Data, c Constants) (State, Data) {
	s.frame = !s.frame

	if !isIn(d, c) {
		return StateAlert{}, d
	}

	return s, d
}

func (s StateSleep) Image(d Data) string {
	frame := "1"
	if s.frame {
		frame = "2"
	}
	return "sleep" + frame
}

type StateItch struct {
	frame      bool
	frameCount int
}

func (s StateItch) Next(d Data, c Constants) (State, Data) {
	s.frame = !s.frame
	s.frameCount++

	if !isIn(d, c) {
		return StateAlert{}, d
	}
	if s.frameCount > c.ItchFrames {
		return StateStill{}, d
	}

	return s, d
}

func (s StateItch) Image(d Data) string {
	frame := "1"
	if s.frame {
		frame = "2"
	}
	return "itch" + frame
}

type StateScratch struct {
	frame      bool
	frameCount int
	direction  string
}

func newStateScratch() StateScratch {
	rand.Seed(time.Now().Unix())
	r := rand.Intn(4)

	var direction string
	switch r {
	case 0:
		direction = DirectionN
	case 1:
		direction = DirectionE
	case 2:
		direction = DirectionW
	case 3:
		direction = DirectionS
	}
	return StateScratch{direction: direction}
}
func (s StateScratch) Next(d Data, c Constants) (State, Data) {
	s.frame = !s.frame
	s.frameCount++

	if !isIn(d, c) {
		return StateAlert{}, d
	}
	if s.frameCount > c.ScratchFrames {
		return StateStill{}, d
	}

	return s, d
}

func (s StateScratch) Image(d Data) string {
	frame := "1"
	if s.frame {
		frame = "2"
	}
	return s.direction + "scratch" + frame
}
