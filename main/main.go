// +build js,wasm

package main

import (
	"fmt"
	"syscall/js"
	"time"

	"github.com/thxi/neko"
)

func main() {
	preloadImages()

	doc := js.Global().Get("document")

	n := setupElement(doc)

	body := doc.Call("querySelector", "body")
	body.Call("appendChild", n)

	var x, y int
	s, d, c := neko.Default()

	go func() {
		// TODO debounce
		doc.Call("addEventListener",
			"mousemove",
			js.FuncOf(func(this js.Value, args []js.Value) interface{} {
				ev := args[0]
				x = ev.Get("y").Int()
				y = ev.Get("x").Int()
				return nil
			}))
	}()

	t := time.NewTicker(300 * time.Millisecond)
	for {
		_ = <-t.C
		p := neko.Point{X: x, Y: y}
		d = neko.UpdateMouseData(d, p)
		s, d = s.Next(d, c)
		updateStyles(n, s, d)
	}

}

func updateStyles(n js.Value, s neko.State, d neko.Data) {
	img := s.Image(d)
	updateImg(n, img)
	updatePos(n, d)
}

func updatePos(n js.Value, d neko.Data) {
	styles := n.Get("style")
	styles.Set("top", fmt.Sprintf("%dpx", d.Neko.X-16))
	styles.Set("left", fmt.Sprintf("%dpx", d.Neko.Y-16))
}

func updateImg(n js.Value, l string) {
	console := js.Global().Get("console")
	url := imgLink(l)

	console.Call("log", url)

	n.Set("src", url)
}

func imgLink(g string) string {
	f := "http://localhost:1337/neko/socks/%s.gif"
	return fmt.Sprintf(f, g)
}

func preloadImages() {
	img := js.Global().Get("Image")
	for _, g := range neko.Gifs {
		t := img.New()
		url := imgLink(g)
		t.Set("src", url)
	}
}

func setupElement(doc js.Value) js.Value {
	e := doc.Call("createElement", "img")
	e.Set("src", `https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fpbs.twimg.com%2Fmedia%2FB8W6wUPCYAEmWjy.png%3Alarge&f=1&nofb=1`)
	styles := e.Get("style")
	styles.Set("position", "absolute")
	styles.Set("top", "-16px")
	styles.Set("left", "-16px")
	return e
}
