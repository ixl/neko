package main

import (
	"fmt"
	"net/http"
	"os"
)

func main() {
	port := os.Getenv("PORT")
	if port == "" {
		port = "1337"
	}
	fmt.Printf("serving on %s\n", port)
	http.ListenAndServe(":"+port, http.FileServer(http.Dir("index")))
}
