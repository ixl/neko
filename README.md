# Neko for the web

Assets stolen from [https://webneko.net/](https://webneko.net/)

Compiles from Go to WebAssembly

# Build
```bash
make build
```
This will generate WA code in *index* directory

```bash
make serve
```
To run locally. Navigate to *localhost:1337* in your web browser.

---
[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)
No tests at all. Use at your own risk

